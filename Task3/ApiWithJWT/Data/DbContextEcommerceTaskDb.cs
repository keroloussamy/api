﻿using ApiWithJWT.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWithJWT.Data
{
    public class DbContextEcommerceTaskDb : DbContext
    {
        public DbContextEcommerceTaskDb(DbContextOptions<DbContextEcommerceTaskDb> options) : base(options)
        {
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Product> Products { get; set; }

        
    }
}
