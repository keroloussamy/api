﻿using ApiWithJWT.Data.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWithJWT.Data
{
    public class DbContextTaskDb : IdentityDbContext<ApplicationUser>
    {
        public DbContextTaskDb(DbContextOptions<DbContextTaskDb> options) : base(options)
        {
        }
    }
}
