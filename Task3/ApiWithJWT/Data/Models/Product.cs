﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWithJWT.Data.Models
{
    public class Product
    {
        public int Id { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string Name { get; set; }

        [Required]
        [StringLength(50, MinimumLength = 2)]
        public string ArabicName { get; set; }
        [Required]
        public float Price { get; set; }
        [Required]
        public int Quanitity { get; set; }
        public string Image { get; set; }
    }
}
