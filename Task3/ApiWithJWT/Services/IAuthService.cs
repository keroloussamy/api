﻿using ApiWithJWT.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiWithJWT.Services
{
    public interface IAuthService
    {
        Task<AuthModel> RegisterAsync(RegisterModel model);
        Task<AuthModel> LoginAsync(LoginModel model);
        Task<AuthModel> ConfirmEmailAsync(string userId, string token);
        Task<AuthModel> ForgetPasswordAsync(ForgotPasswordModel forgotPasswordModel);
        Task<AuthModel> ResetPasswordAsync(ResetPasswordModel resetPasswordModel);
    }
}
